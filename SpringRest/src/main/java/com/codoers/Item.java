package com.codoers;

public class Item implements Cloneable{
    private int id;
    private int orderNo;
    private float latitude;
    private float longitude;
    private int assignedCenterId = -1;
    private String color = null;

    public Item(){}

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public Item(int id, float latitude, float longitude, String color) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.color = color;
    }
    public Item(int id, int orderNo, float latitude, float longitude, int assignedCenterId, String color) {
        this.id = id;
        this.orderNo = orderNo;
        this.latitude = latitude;
        this.longitude = longitude;
        this.assignedCenterId = assignedCenterId;
        this.color = color;
    }

    public Item(int id, float latitude, float longitude) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Item(int id, float latitude, float longitude, int assignedCenterId) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.assignedCenterId = assignedCenterId;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", orderNo=" + orderNo +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", assignedCenterId=" + assignedCenterId +
                ", color='" + color + '\'' +
                '}';
    }

    public Item(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getAssignedCenterId() {
        return assignedCenterId;
    }

    public void setAssignedCenterId(int assignedCenterId) {
        this.assignedCenterId = assignedCenterId;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(int orderNo) {
        this.orderNo = orderNo;
    }
}
