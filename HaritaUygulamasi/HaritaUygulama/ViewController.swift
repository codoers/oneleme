//
//  ViewController.swift
//  HaritaUygulama
//
//  Created by Beyza Canbay on 25.02.2019.
//  Copyright © 2019 Beyza Canbay. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

struct ResponseObject: Codable {
    let data: [Customer]
}
struct Customer : Codable{
    let color : String;
    let id : Int;
    let orderNo: Int;
    let assignedCenterId: Double;
    let latitude : Double;
    let longitude : Double;
    
}

class MyPointAnnotation : MKPointAnnotation {
    var pinTintColor: UIColor?
}


class ViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    var customersArray : Array<Customer> = []
    //var customersArray:[Customer]?
    
    @IBOutlet weak var harita: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        harita.delegate = self
        
        
        
        let hello = MyPointAnnotation()
        hello.coordinate = CLLocationCoordinate2D(latitude: 41.049792, longitude: 29.003031)
        hello.title = "Kırmızı Bayi"
        hello.pinTintColor = .black
        
        let hellox = MyPointAnnotation()
        hellox.coordinate = CLLocationCoordinate2D(latitude: 41.069940, longitude: 29.019250)
        hellox.title="Mavi Bayi"
        hellox.pinTintColor = .black
        
        let hello2 = MyPointAnnotation()
        hello2.coordinate = CLLocationCoordinate2D(latitude:41.049997, longitude:29.026108)
        hello2.title="Yesil Bayi"
        hello2.pinTintColor = .black
        
        
        
        
        self.harita.addAnnotation(hello)
        self.harita.addAnnotation(hellox)
        self.harita.addAnnotation(hello2)
        
        
        let url = URL(string: "http://buraktahtaci:9000/")
        
        let task = URLSession.shared.dataTask(with: url!) {(data, response, error) in
            guard error == nil else {
                print("returning error")
                return
            }
            
            guard let content = data else {
                print("not returning data")
                return
            }
            
            let decoder = JSONDecoder()
            do {
                let responseObject = try decoder.decode(ResponseObject.self, from: content)
                let customers = responseObject.data
                
                for cust in customers{
                    print(cust)
                    let hello = MyPointAnnotation()
                    hello.coordinate = CLLocationCoordinate2D(latitude: cust.latitude, longitude: cust.longitude)
                    if cust.color == "red"{
                        hello.pinTintColor = .red
                    }
                    if cust.color == "green"{
                        hello.pinTintColor = .green
                    }
                    if cust.color == "blue"{
                        hello.pinTintColor = .blue
                    }
                    self.harita.addAnnotation(hello)
                    
                }
                
            } catch {
                print(error)
            }
            
        }
        
        task.resume()
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "myAnnotation") as? MKPinAnnotationView
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "myAnnotation")
        } else {
            annotationView?.annotation = annotation
        }
        
        if let annotation = annotation as? MyPointAnnotation {
            annotationView?.pinTintColor = annotation.pinTintColor
            annotationView?.canShowCallout = true
        }
        
        return annotationView
    }
    func retrieveData(){
        parseJSON()
    }
    func parseJSON() {
        let url = URL(string: "http://192.168.1.105:5000/")
        
        let task = URLSession.shared.dataTask(with: url!) {(data, response, error) in
            guard error == nil else {
                print("returning error")
                return
            }
            
            guard let content = data else {
                print("not returning data")
                return
            }
            
            let decoder = JSONDecoder()
            do {
                let responseObject = try decoder.decode(ResponseObject.self, from: content)
                print(type(of: responseObject.data) )
                self.customersArray = responseObject.data
                
            } catch {
                print(error)
            }
            
            return
            
            
        }
        
        task.resume()
        
    }
    
}
