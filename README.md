## Problemin Tanımı

Hackathonun ön eleme aşaması olarak verilen soruda 100 adet pakedin, 3 adet çiçek bayisine kotaları bazında optimum şekilde dağıtılması problemi anlatılmıştır. Bu bağlamda kotalar optimum şekilde dağıtılırken gidilen mesafelerin de minimize edildiği bir algoritma geliştirilmiştir. Bu algoritma Java programlama diliyle gerçekleştirilmiş olup REST API üzerinden sonucu döndürmektedir. Swift ile yazılmış native bir iOS uygulaması da bu servisten hizmet alarak paketlerin yerini gösterecek şekilde tasarlanmıştır.

Problemin çözümünde dikkat edilen en önemli husus bayilerin kota aralıkları olmuştur ve paketler o aralıklar uyarınca dağıtılmıştır. 

---

## Algoritmik Yaklaşım
Verilen bayilerin kapasitelerinin ve sipariş noktalarına gidilecek toplam mesafelerin optimize edilmesi gerekmektedir. Sunulan çözümde problem bir optimizasyon problemi olarak ele alınmıştır ve sezgisel arama algoritması tasarlanarak çözülmüştür. 

- Optimizasyon algoritmalarında başlangıç noktası,  step size ve optimizasyon kriterleri(kota aralığı ve toplam gidilecek mesafe) tasarlanan değerlendirme fonksiyonunda kullanılmıştır. 
- Öncelikle başlangıç kotaları verilen aralıklara uygun olacak ve toplam sipariş noktası kriterine uyacak adette göreli olarak belirlenmiştir. 
- Sonrasında ise noktaların bayilere uzaklıkları bulunarak, dengeli bir değerlendirme yapılabilmesi bakımından normalize edilmiştir.
- Belirlenen bayilerin kotaları ve mesafeler değerlendirme fonksiyonunda ağırlıklandırılarak kullanılmıştır.
- Değerlendirme fonksiyonu : 
    - Her bir sipariş noktası için 
        - **(İlgili bayiye olan normalize edilmiş uzaklık *  mesafe ağırlığı) + (bayiye atanmış nokta sayısı/bayinin başlangıç kapasitesi) * kapasite ağırlığı**  olacak şekilde tasarlanmıştır. Değerlendirme fonksiyonu açıklanacak olunursa, herbir sipariş noktasının bayiye olan uzaklığı ve bayinin doluluk oranına göre atanma kriterlerini göz önünde bulundurarak değerlendirmektedir. Değerlendirme fonksiyonuna göre bir sipariş noktasının bayiye atanma maliyeti mesafe ve bayi kapasitesi bakımından iteratif olarak değerlendirilmekte ve gelen her bir nokta için maliyeti en az olan bayi ataması yapılmaktadır. 
        - Kotalara alt ve üst limitler bakımından uyan ve toplam mesafe bakımından en optimum olan iterasyondaki atamalar ise çözümün çıktısı olarak sunulmaktadır.

---

## Elde Edilen Sonuçlar


**Kırmızı Bayi:**
Sipariş IDleri: [104, 110, 113, 114, 118, 119, 120, 121, 122, 123, 124, 126, 137, 145, 153, 156, 159, 166, 168, 169, 170, 172, 174, 182, 184, 187]
Sipariş Sayısı: 26
Siparişlere Uzaklıklar Toplamı: 22.61km

**Yeşil Bayi:**
Sipariş IDleri: [100, 101, 102, 103, 105, 106, 108, 109, 111, 112, 116, 125, 127, 128, 129, 130, 131, 132, 134, 136, 139, 141, 142, 146, 147, 149, 151, 154, 157, 158, 160, 162, 163, 167, 171, 175, 177, 178, 179, 180, 181, 185, 186, 189, 190, 192, 193, 194, 195, 198]
Sipariş Sayısı: 50
Siparişlere Uzaklıklar Toplamı:66.16km

**Mavi Bayi:**
Sipariş IDleri: [107, 115, 117, 133, 135, 138, 140, 143, 144, 148, 150, 152, 155, 161, 164, 165, 173, 176, 183, 188, 191, 196, 197, 199]
Sipariş Sayısı: 24
Siparişlere Uzaklıklar Toplamı: 34.98km

**Toplam Alınan Mesafe:** 123.74km
---

## Verilen Görselleştirilmesi

Siparişler kota ve minimum yol alma bazında minimize edilerek oluşturulmuş sonuçların bulunduğu servisten hizmet alan bır iOS uygulaması tasarlanmıştır. Uygulama temel olarak bir MapKit haritası üzerinde bulunan farklı renklerde Annotation'lardan oluşmaktadır. Kırmızı, Yeşil ve Mavi bayiler için uygun renkte pinler kullanılmıştır. Bunun yanında bayilerin bulunduğu noktalar siyah pinle işaretlenmiştir.
 Ana Dağılım:
 ![picture](screenshots/distribution.png)

 Kırmızı Bayiler:
 ![picture](screenshots/red.png)

 Yeşil Bayiler
 ![picture](screenshots/green.jpeg)

 Mavi Bayiler
 ![picture](screenshots/blue.png)

---
