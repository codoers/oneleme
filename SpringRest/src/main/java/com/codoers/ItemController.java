package com.codoers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ItemController {
    private ItemMockedData itemMockedData = ItemMockedData.getInstance();

    public ItemController() throws CloneNotSupportedException {
    }

    @RequestMapping(value="/", method = RequestMethod.GET)
    public ResponseEntity< ObjectResponse > get() {
        ObjectResponse instance = new ObjectResponse(200, "OK", itemMockedData.resultOrders);
        return new ResponseEntity< ObjectResponse >(instance, HttpStatus.OK);
    }

    @RequestMapping(value = "/query", method = RequestMethod.POST)
    public ResponseEntity<Item> post(@RequestBody Item item) {
        Item itemResponse = null;
//        System.out.println("Order id :  " + item.getId());
        if (item != null) {
            itemResponse = itemMockedData.getItemById(item.getId());
        }

        // TODO: call persistence layer to update
        return new ResponseEntity<Item>(itemResponse, HttpStatus.OK);
    }
}