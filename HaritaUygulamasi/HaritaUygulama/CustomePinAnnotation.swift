//
//  CustomePinAnnotation.swift
//  HaritaUygulama
//
//  Created by Beyza Canbay on 27.02.2019.
//  Copyright © 2019 Beyza Canbay. All rights reserved.
//

import UIKit
import MapKit

class CustomePinAnnotation: MKPointAnnotation {
    
    var pinColor: UIColor!
    
    var pinImage:String!
    var annotations:MKPointAnnotation!
}
