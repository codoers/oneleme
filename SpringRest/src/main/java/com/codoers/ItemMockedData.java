package com.codoers;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.ClassPathResource;

import java.io.*;
import java.util.*;
import java.util.stream.IntStream;


public class ItemMockedData {

    private List<Item> items;
    public List<Item> centers ;
    public List<Item> orders;
    public List<Item> resultOrders;
    String [] colornames = new String[]{"red", "green", "blue"};
    private int totalItemNumber = 1000;
    public String FILE_NAME = "Excel/siparis ve bayi koordinatları.xlsx";
    public int[][] boundaries = new int[][] {{20,30},{35,50},{20,80}};
    public float weightCapacityPoint;
    public float weightDistancePoint = (float) 0.0;
    public float weightIncrementValue = (float) 0.0001;
    public float normalizingFactor = (float) 100;
    public int [] assignCounts = {0,0,0};
    public float [] totalDistances = {0,0,0};
    public ArrayList<ArrayList<Float>> distances = new ArrayList<ArrayList<Float>>();
    public ArrayList<ArrayList<Float>> distancesNonNorm = new ArrayList<ArrayList<Float>>();
    public int[] startLimits = {0,0,0};
    public ArrayList<ArrayList<Integer>> assigning = null;


    private static ItemMockedData instance = null;

    public static ItemMockedData getInstance() throws CloneNotSupportedException {
        if (instance == null)
            instance = new ItemMockedData();
        instance.calculateDistanceMatrix();
        instance.calculateStartLimits(instance.orders.size(), (float)0.1,0);
        instance.assignOrdersToCenters();
        instance.assignColorNameToOrders();
        return instance;
    }

    private static double getRandomDoubleBetweenRange(double min, double max) {
        double x = (Math.random() * ((max - min) + 1)) + min;
        return x;
    }

    public ItemMockedData() {
        items = new ArrayList<Item>();
        for (int i = 0; i < totalItemNumber; i++) {
            float randLat = (float) getRandomDoubleBetweenRange(41.041840000000, 41.092640000000);
            float randLong = (float) getRandomDoubleBetweenRange(28.987130000000, 29.045160000000);
            items.add(new Item(i, randLat, randLong));
        }
    }

    public List<Item> fetchItems() {
        return items;
    }

    public Item getItemById(List<Item> items, int id) {
        for (Item item : items) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }

    public Item createItem(int id, float lat, float lon) {
        Item item = new Item(id, lat, lon);
        return item;
    }

    public Item updateItemById(int idOld, int idNew, float lat, float lon) {
        for (Item item : items) {
            if (item.getId() == idOld) {
                item.setLatitude(lat);
                item.setLongitude(lon);
                item.setId(idNew);
                return item;
            }
        }
        return null;
    }

    public Item assignItemCentralId(int id, int centralId) {
        for (Item item : items) {
            if (item.getId() == id) {
                item.setAssignedCenterId(centralId);
                return item;
            }
        }
        return null;
    }

    public List<Item> readCenterItemFromXlsxFile() {
        ArrayList <Item> items = new ArrayList<Item>();
        int i = 1;
        try {
            File file = new ClassPathResource(FILE_NAME).getFile();
            FileInputStream excelFile = new FileInputStream(file);
            Workbook workbook = new XSSFWorkbook(excelFile);

            Sheet datatypeSheet = workbook.getSheetAt(1);
            Iterator<Row> iterator = datatypeSheet.iterator();
//            Skipping Header
            Row currentRow = iterator.next();
            while (iterator.hasNext()) {
                Item item = new Item();
                item.setId(i);
                currentRow = iterator.next();
                i += 1;
                Iterator<Cell> cellIterator = currentRow.iterator();
                Cell currentCell = cellIterator.next();
                item.setColor(currentCell.getStringCellValue());
                currentCell = cellIterator.next();
                item.setLatitude(Float.parseFloat(currentCell.getStringCellValue()));
                currentCell = cellIterator.next();
                item.setLongitude(Float.parseFloat(currentCell.getStringCellValue()));
                items.add(item);
//                System.out.println(item.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return items;
    }

    public List<Item> readOrderItemFromXlsxFile(){
        ArrayList <Item> items = new ArrayList<Item>();
        int i = 1;
        try {
            File file = new ClassPathResource(FILE_NAME).getFile();
            FileInputStream excelFile = new FileInputStream(file);
            Workbook workbook = new XSSFWorkbook(excelFile);

            Sheet datatypeSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = datatypeSheet.iterator();
//            Skipping Header
            Row currentRow = iterator.next();
            while (iterator.hasNext()) {
                Item item = new Item();
                item.setId(i);
                currentRow = iterator.next();
                i += 1;
//                currentRow = iterator.next();
                Iterator<Cell> cellIterator = currentRow.iterator();
                Cell currentCell = cellIterator.next();
//                System.out.println(currentCell.getStringCellValue());
                item.setOrderNo(Integer.parseInt(currentCell.getStringCellValue()));
                currentCell = cellIterator.next();
                item.setLatitude(Float.parseFloat(currentCell.getStringCellValue()));
                currentCell = cellIterator.next();
                item.setLongitude(Float.parseFloat(currentCell.getStringCellValue()));
                items.add(item);
//                System.out.println(item.toString());
//                item.toString();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return items;

    }

    public int[] calculateStartLimits(int total_order, float increment, float ratio){
        int determinedMaxLimit =  IntStream.of(startLimits).sum();
        while ((determinedMaxLimit < total_order) && (ratio < 1)){
            for (int i=0; i < boundaries.length; i++)
                startLimits[i] = (int) (boundaries[i][0] + ((boundaries[i][1] - boundaries[i][0]) * ratio));
            determinedMaxLimit =  IntStream.of(startLimits).sum();
            ratio += increment;
            if (ratio >= 1 ){
                System.out.println("No enough capacity for optimizing according to maximum capacity.Assigning max values");
                for (int i=0; i<boundaries[0].length; i++)
                    startLimits[i] = boundaries[i][1];
            }
        }
//        System.out.println(Arrays.toString(startLimits));
        return startLimits;
    }

    public ArrayList<ArrayList <Integer>> assignOrdersToCenters() throws CloneNotSupportedException {
        ArrayList<Float> scores;
        ArrayList<ArrayList<Integer>> group = null;
        float minStop = Float.MAX_VALUE;
        int control = 0;
        while (weightDistancePoint<=1){
            weightDistancePoint += weightIncrementValue;
            weightCapacityPoint = 1 - weightDistancePoint;
            assignCounts = new int[]{0, 0, 0};
            totalDistances = new float[]{0, 0, 0};
            for (int i=0 ; i <distances.get(0).size();i++){
                scores = new ArrayList<Float>();
                for (int j=0;j<distances.size();j++)
                    scores.add(distances.get(j).get(i)*weightDistancePoint + ((float) assignCounts[j] / (float) startLimits[j])*weightCapacityPoint);
                orders.get(i).setAssignedCenterId(scores.indexOf(Collections.min(scores)));
//                System.out.println(orders.get(i).toString());
                assignCounts[(int) orders.get(i).getAssignedCenterId()] +=1;
                totalDistances[(int) orders.get(i).getAssignedCenterId()] += distancesNonNorm.get((int) orders.get(i).getAssignedCenterId()).get(i);
            }
            float tmp = sum(totalDistances);
            if (assignCounts[0]>= boundaries[0][0] && assignCounts[0]<=boundaries[0][1] &&
            assignCounts[1]>= boundaries[1][0] && assignCounts[1]<=boundaries[1][1] &&
            assignCounts[2]>= boundaries[2][0] && assignCounts[2]<=boundaries[2][1] && tmp<minStop){
                group = new ArrayList<ArrayList<Integer>>();
                for (int i=0; i<distances.size(); i++)
                    group.add(new ArrayList<Integer>());
                for (Item order: orders){
                    group.get((int) order.getAssignedCenterId()).add(order.getId()-1);
                }
                for (int i=0; i<group.size(); i++)
                    System.out.println(Arrays.toString(new ArrayList[]{group.get(i)}));
                System.out.println(Arrays.toString(totalDistances));
                System.out.println(sum(totalDistances));
                System.out.println(Arrays.toString(assignCounts));
                minStop=tmp;
                resultOrders = new ArrayList<Item>();
//                for (Item order : orders){
//                    Item tmpItem = (Item) deepCopy(order);
//                    resultOrders.add(tmpItem);
//                }
                Iterator<Item> iterator = orders.iterator();

                while(iterator.hasNext())
                {
                    //Add the object clones
                    resultOrders.add((Item) iterator.next().clone());
                }

            }
//            System.out.println(Arrays.toString(new ArrayList[]{(ArrayList) scores}));
//                int indexOfMinimum = list.indexOf(Collections.min(list));
        }
        assigning = group;
        return group;
    }

    public ArrayList<ArrayList<Float>> calculateDistanceMatrix(){
//        float max = (float) 0.0;
        centers = readCenterItemFromXlsxFile();
        orders = readOrderItemFromXlsxFile();
        for (Item c : centers) {
            ArrayList<Float> cDist= new ArrayList<Float>();
            ArrayList<Float> cDistNonNorm= new ArrayList<Float>();
            for (Item o : orders){
                float distanceTmp = calculateDistance(c,o);
                cDist.add(distanceTmp/normalizingFactor);
                cDistNonNorm.add(distanceTmp);
//                if(distanceTmp>max)
//                    max = distanceTmp;
            }
            distancesNonNorm.add(cDistNonNorm);
            distances.add(cDist);
        }
        System.out.println(Arrays.toString(new ArrayList[]{distances}));
        return distances;
    }

    public void assignColorNameToOrders(){
        for (Item order: resultOrders){
            order.setColor(colornames[(int) order.getAssignedCenterId()]);
        }
    }

    public double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    public double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    public float calculateDistance(Item i1, Item i2) {
        Double lat1 = (double) i1.getLatitude();
        Double lon1 = (double) i1.getLongitude();
        Double lat2 = (double) i2.getLatitude();
        Double lon2 = (double) i2.getLongitude();
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515 * 1.609344;

        return (float) dist;
    }

    public Item getItemById(int i) {
        for (Item item : items) {
            if (item.getId() == i)
                return item;
        }
        return null;
    }

    public float sum(float[] array) {
        float sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        return sum;
    }


}
