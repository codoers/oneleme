package com.codoers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServiceController {
    @RequestMapping("/index")
    public String index(){
        return "Welcome to api service springrest";
    }
}
